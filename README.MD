# TASK MANAGER

## DEVELOPER INFO

* **Name**: Kuzin Igor

* **E-mail**: garbir9@mail.ru

## SOFTWARE

* **OS**: Windows 10.0.17763

* **JAVA**: OpenJDK 1.8.0_345

## HARDWARE

* **CPU**: AMD Ryzen 5 3500U

* **RAM**: 16GB

* **SSD**: 512GB

## PROGRAM RUN

```bash
java -jar ./task-manager.jar
```